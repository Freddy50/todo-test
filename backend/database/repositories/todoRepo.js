import { Todo } from "../model/Todo.js";

export default class TodoRepo {
  // create a new todo
  static async create(todo) {
    try {
      return await Todo.create(todo);
    } catch (error) {
      throw error;
    }
  }

  // return all todos
  static async findAll() {
    try {
      return await Todo.find({}).sort({ createdAt: "desc" });
    } catch (error) {
      throw error;
    }
  }

  // find todo by the id
  static async findById(id) {
    try {
      return await Todo.findById(id);
    } catch (error) {
      throw error;
    }
  }

  // update todo
  static async updateById(id, todo) {
    try {
      return await Todo.updateOne({ _id: id }, { $set: { ...todo } });
    } catch (error) {
      throw error;
    }
  }

  // delete todo
  static async deleteById(id) {
    try {
      return await Todo.findByIdAndDelete(id);
    } catch (error) {
      throw error;
    }
  }
}
